import { createWebHistory, createRouter } from "vue-router";

import Index from "./pages/Index.vue";
import Resume from "./pages/Resume.vue";

const routes = [
  { path: "/resume/", component: Resume },
  { path: "/", component: Index },
];

const router = createRouter({
  mode: "history",
  history: createWebHistory(),
  routes,
});

export default router;
