# Personal Website V3

This is my personal portfolio website made with Vue and Vite.

## How to run

```bash
# Install dependencies
npm install

# To run in dev mode with hot reload
npm run dev

# To build
npm run build

# To preview build
npm run preview
```

## TODO

- [ ] Fix self-hosted link. Server is currently down while I am travelling!
- [ ] Create playground for small front-end projects.

## CI/CD

This repository is configured to automatically build and deploy on commits to the master branch. The site itself is hosting in an AWS bucket set up for static site hosting. The deploy stage uses the AWS CLI to sync the build artifact with the S3 bucket. The site also uses CloudFront for the SSL certificate to provide HTTPS.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).
