/** @type {import('tailwindcss').Config} */

const colors = require("tailwindcss/colors");

export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      boxShadow: {
        custom: `2px 2px 0`,
        "custom-hover": `1px 1px 0`,
      },
      colors: {
        primary: colors.blue,
        // primary: "rgb(var(--color-primary))",
        secondary: "rgb(var(--color-secondary))",
      },
    },
  },
  plugins: [],
};
